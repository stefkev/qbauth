const express = require('express');
const OAuthClient = require('intuit-oauth');
const util = require('../lib').util;
const authUtil = require('../lib').authenticate;
const intervalReAuth = require('../lib/intervalscan').intervalReAuth; 
const {ERROR_CODES, errorHandler}  = require('../lib/errorHandler');
let router = express.Router();

const QB_CLIENT_ID = process.env.QB_CLIENT_ID;
const QB_CLIENT_SECRET = process.env.QB_CLIENT_SECRET;
const QB_ENVIRONMENT = process.env.QB_ENVIRONMENT || 'sandbox';
const QB_REDIRECT = process.env.QB_REDIRECT;


let oauthClient = new OAuthClient({
  clientId: QB_CLIENT_ID,
  clientSecret: QB_CLIENT_SECRET,
  environment: QB_ENVIRONMENT,
  redirectUri: QB_REDIRECT,
  logging: false
});


router.get('/auth', (req, res) => {
  let authUri = oauthClient.authorizeUri(
    {scope:[OAuthClient.scopes.Accounting,OAuthClient.scopes.OpenId],
      state:'testState'});
  res.redirect(authUri);
});

router.get('/authredir', (req, res) => {
  let parseRedirect = req.url;

  oauthClient.createToken(parseRedirect)
    .then(function(authResponse) {
      let data = authResponse.getJson();
      Object.assign(util.storeEnum.stored.optics, data);
      let qbo =  authUtil.authenticate(data.access_token, data.refresh_token);
      Object.assign(qbo, util.qbo);
      intervalReAuth(3600);
      res.json(authResponse.getJson())

    })
    .catch(function(e) {
      console.error("The error message is :"+e.originalMessage);
      console.error(e.intuit_tid);
      errorHandler(ERROR_CODES.AUTH_CONNECT);
      res.json({
        id: e.intuit_tid,
        message: e.originalMessage
      })
    });
});

router.get('/refresh', (req, res) => {
  oauthClient.refresh()
    .then(function(authResponse) {
      res.json(authResponse.json);
    })
    .catch(function(e) {
      console.error("The error message is :"+e.originalMessage);
      console.error(e.intuit_tid);
      errorThrow(ERROR_CODES.AUTH_CONNECT);
      res.json({
        id: e.intuit_tid,
        message: e.originalMessage
      })
    });
})

router.get('/revoke', (req, res) => {
  oauthClient.setToken(oauthClient.token.getToken());
  oauthClient.revoke()
    .then(function(authResponse) {
      if(authResponse)
        res.json({revoked: true});
    })
    .catch(function(e) {
      console.error("The error message is :"+e.originalMessage);
      console.error(e.intuit_tid);
      errorHandler(ERROR_CODES.AUTH_CONNECT, e, null);
      res.json({
        id: e.intuit_tid,
        message: e.originalMessage
      })
    });
});

router.get('/validate', (req, res) => {
  oauthClient.validateIdToken()
    .then(function(response){
      res.json({valid: response})
    })
    .catch(function(e) {
      errorThrow(ERROR_CODES.AUTH_CONNECT, e, null);
      res.json({error: JSON.stringify(e)})
    });
});


module.exports = {
  router,
  oauthClient
};
