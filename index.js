const express = require('express');
const fs = require('fs');
const https = require('https');
const http = require('http');
let app = express();
require('dotenv').config();

const intervalScan = require('./lib/intervalscan').intervalScan;

const HTTP_PORT = process.env.HTTP_PORT || 3000;
const HTTPS_PORT = process.env.HTTPS_PORT || 443;

app.use('/oauth', require("./routes/qbOauth").router);

certOptions = {
  key: fs.readFileSync(__dirname + '/cert/qbvirtuerx.key'),
  cert: fs.readFileSync(__dirname + '/cert/qbvirtuerx.crt'),
  ca: fs.readFileSync(__dirname + '/cert/qbvirtuerx-bundle.ca-bundle')
};

https.createServer(certOptions, app).listen(HTTPS_PORT, (err) => {
  if (err) {
    console.error(err);
  }
  console.log('HTTPS started on port 443')
});

http.createServer(app).listen(HTTP_PORT, (err) => {
  if(err){
    console.error(err);
  }
  console.log(`Server started on port ${HTTP_PORT}`);
});


intervalScan();


/*
fmazza@virtuerx.com Chirico456!
 */
