const {
  items,
  customers,
  purchaseOrder,
  invoice,
  vendor
} = require('./quickbooks');
const {
  QBCompanyModel,
  QBItemModel,
  QBTranslogMSTModel,
  QBTranslogDTLModel,
  QBRequestsModel
} = require('./database');
const stored = require('../lib').util.storeEnum.stored.optics;
const {
  ERROR_CODES,
  errorHandler
} = require('../lib/errorHandler');

const moment = require('moment');


const ACCOUNT_NAME = 'VIRTUE';

async function pushOrder(request_uid) {
  try {
    let logData = await logFetcher(request_uid);
    await purchaseOrder.createPurchaseOrder(logData);
    await completeRequest(request_uid);
  } catch (err) {
    await errorHandler(err.code, err.message, request_uid);
  }
}


async function pushInvoice(request_uid, className) {
  try {
    let logData = await logFetcher(request_uid);
    let invoicePush = await invoice.createInvoice(logData, className);
    await completeRequest(request_uid);
  } catch (err) {
    await errorHandler(err.code, err.message, request_uid);
  }
}


async function vendorDownload(request_id) {
  try {
    let vendorData = await vendor.findVendors();
    let output = vendorData.QueryResponse.Vendor;
    output.map(vendorData => {
      let account_name = ACCOUNT_NAME;
      let address = "";
      let city = "";
      let state = "";
      let postal = "";

      if (vendorData.hasOwnProperty('BillAddr')) {
        if (vendorData.BillAddr.hasOwnProperty('Line1')) {
          address = vendorData.BillAddr.Line1;
        }
        if (vendorData.BillAddr.hasOwnProperty('City')) {
          city = vendorData.BillAddr.City;
        }
        if (vendorData.BillAddr.hasOwnProperty('CountrySubDivisionCode')) {
          state = vendorData.BillAddr.CountrySubDivisionCode;
        }
        if (vendorData.BillAddr.hasOwnProperty('PostalCode')) {
          postal = vendorData.BillAddr.PostalCode;
        }
      }
      let company_name = vendorData.DisplayName;
      let vendor = 1;
      
      let external_id = parseInt(vendorData.Id);
      QBCompanyModel.findOrCreate({
        where: {
          rec_uid: vendorData.Id
        },
        defaults: {
          address,
          account_name,
          company_name,
          city,
          state,
          postal,
          vendor,
          
          external_id,
        }
      }).then(created => {
        console.log('created vendor')
      }).catch(err => {
        throw {
          code: ERROR_CODES.DATABASE_ERROR,
          messsage: e
        }
      })
    });
    await completeRequest(request_id)
  } catch (err) {
    await errorHandler(err.code, err.message, request_id);
  }
}
async function customerDownload(request_id) {
  try {
    let customerData = await customers.findAll();
    let output = customerData.QueryResponse.Customer;
    output.map(customer => {
      let account_name = ACCOUNT_NAME;
      let address = "";
      let city = "";
      let state = "";
      let postal = "";
      if (customer.hasOwnProperty('BillAddr')) {
        if (customer.BillAddr.hasOwnProperty('Line1')) {
          address = customer.BillAddr.Line1;
        }
        if (customer.BillAddr.hasOwnProperty('City')) {
          city = customer.BillAddr.City;
        }
        if (customer.BillAddr.hasOwnProperty('CountrySubDivisionCode')) {
          state = customer.BillAddr.CountrySubDivisionCode;
        }
        if (customer.BillAddr.hasOwnProperty('PostalCode')) {
          postal = customer.BillAddr.PostalCode;
        }
      }

      let company_name = customer.DisplayName;
      let vendor = 0;
      
      let external_id = parseInt(customer.Id);

      QBCompanyModel.findOrCreate({
        where: {
          rec_uid: customer.Id
        },
        defaults: {
          address,
          account_name,
          company_name,
          city,
          state,
          postal,
          vendor,
          external_id,
        }
      }).then(created => {
        console.log('created customer')
      }).catch(err => {
        throw {
          code: ERROR_CODES.DATABASE_ERROR,
          message: e
        }
      });
    });
    await completeRequest(request_id);
  } catch (err) {
    await errorHandler(err.code, err.message, request_id)
  }
}

async function itemDownload(request_id) {
  try {
    let itemData = await items.findAll();
    let output = itemData.QueryResponse.Item;
    output.map(item => {
      let account_name = 'VIRTUE';
      let item_desc = item.Description;
      let upc_number = item.Name;
      let external_id = item.Id;
      QBItemModel.findOrCreate({
        where: {
          rec_uid: item.Id
        },
        defaults: {
          account_name,
          item_desc,
          upc_number,
          external_id
        }
      }).then(created => {
        console.log('created item')
      }).catch(err => {
        throw {
          code: ERROR_CODES.DATABASE_ERROR,
          message: e
        }
      });
    });
    await completeRequest(request_id);
  } catch (err) {
    await errorHandler(err.code, err.message, request_id)
  }
}

async function completeRequest(requestID) {
  QBRequestsModel.update({
    status: 1,
    error_msg: ""
  }, {
    where: {
      request_uid: requestID
    }
  }).then(requestData => {
    console.log(`Completed request with id ${requestID}`);
  }).catch(err => {
    throw {
      code: ERROR_CODES.DATABASE_ERROR,
      message: err
    }
  })
}


async function logFetcher(requestID) {
  return new Promise((resolve, reject) => {
    return QBTranslogMSTModel.findOne({
      where: {
        request_uid: requestID
      }
    }).then(mstData => {
      if (!mstData) {
        return reject({
          code: ERROR_CODES.MISSING_DATA,
          message: `Missing MST data on request ID: ${requestID}`
        });
      }
      return resolve({
        mst: {
          order_uid: mstData.get('order_uid'),
          company_uid: mstData.get('company_uid'),
          external_id: mstData.get('external_id'),
          total_amount: mstData.get('total_amount'),
          order_date: mstData.get('order_date'),
          ref_id: mstData.get('ref_id'),
          doc_id: mstData.get('doc_id'),
          terms: mstData.get('terms')
        }
      });
    })
  }).then(mstData => {
    const company_uid = mstData.mst.company_uid;
    return QBCompanyModel.findOne({
      where: {
        rec_uid: mstData.mst.external_id
      },
    }).then(clientData => {
      if (!clientData) {
        throw {
          code: ERROR_CODES.MISSING_DATA,
          message: `Missing client company data with company id ${company_uid}, under request id ${requestID}`
        };
      }
      return {
        company: {
          company_name: clientData.get('company_name'),
          external_id: clientData.get('external_id'),
        },
        mstData
      };
    })
  }).then(clientData => {
    return QBTranslogDTLModel.findAll({
      where: {
        request_uid: requestID
      }
    }).then(dtlData => {
      if (!dtlData) {
        console.log(`No DTL data found on record ${requestID}`);
        throw {
          code: ERROR_CODES.MISSING_DATA,
          message: `No DTL found with request id ${requestID}`
        };
      }

      async function processDtl() {
        let dtlOverall = [];
        for (const dtl of dtlData) {
          let upc_number = dtl.get('upc_number');
          let itemFound = QBItemModel.findOne({
            where: {
              upc_number: upc_number
            }
          }).then(item => {
            if (!item) {
              return null
            }
            return {
              external_id: item.get('external_id'),
              desc: item.get('item_desc')
            }
          });
          let item = await itemFound;
          if (!item) {
            try {
              const dateCreated = moment(clientData.mstData.mst.order_date).format('YYYY-MM-DD')
              let createdItem = await items.create(dtl.product_desc, 'Inventory', dtl.qty, dtl.upc_number, dtl.price, dateCreated);
              QBItemModel.findOrCreate({
                where: {
                  rec_uid: createdItem.Id
                },
                defaults: {
                  account_name: 'VIRTUE',
                  item_desc: createdItem.Description,
                  upc_number: createdItem.Name,
                  external_id: createdItem.Id,
                  entered: createdItem.InvStartDate
                }
              }).then(created => {
                console.log('created item')
              }).catch(err => {
                throw {
                  code: ERROR_CODES.DATABASE_ERROR,
                  message: err
                }
              });
              item = {
                external_id: createdItem.Id,
                desc: createdItem.Description
              }
            } catch (err) {
              throw err;
            }

          }
          let packedData = {
            upc_number: dtl.get('upc_number'),
            product_desc: dtl.get('product_desc'),
            qty: dtl.get('qty'),
            extprice: dtl.get('extprice'),
            price: dtl.get('price'),
            lineitem: dtl.get('lineitem'),
            item
          };
          dtlOverall.push(packedData)
        }
        return dtlOverall
      }
      return processDtl().then((dtl) => {
        return {
          dtl,
          mst: clientData.mstData.mst,
          company: clientData.company
        };
      })
    })
  })
}

module.exports = {
  completeRequest,
  pushInvoice,
  pushOrder,
  vendorDownload,
  customerDownload,
  itemDownload
};
