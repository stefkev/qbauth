const {
  QBRequestsModel
} = require('./database');

const ERROR_CODES = {
  CUSTOMER_EXTERNAL_ID: '-1',
  VENDOR_EXTERNAL_ID: '-2',
  PRODUCT_EXTERNAL_ID: '-3',
  API_ERROR: '-7',
  AUTH_CONNECT: '-10',
  MISSING_DATA: '-16',
  DATABASE_ERROR: '-20'
};

async function errorHandler(error, errorMessage, requestID) {
  const msg = errorMessage || "";
  switch (error) {
    case ERROR_CODES.CUSTOMER_EXTERNAL_ID: {
      errorWritter(ERROR_CODES.CUSTOMER_EXTERNAL_ID, 'Missing customer external ID');
      await voidRequest(ERROR_CODES.CUSTOMER_EXTERNAL_ID, 'Missing customer external ID', requestID)
      break;
    }
    case ERROR_CODES.VENDOR_EXTERNAL_ID: {
      errorWritter(ERROR_CODES.VENDOR_EXTERNAL_ID, 'Missing vendor external ID');
      await voidRequest(ERROR_CODES.VENDOR_EXTERNAL_ID, 'Missing vendor external ID', requestID);
      break;

    }
    case ERROR_CODES.PRODUCT_EXTERNAL_ID: {
      errorWritter(ERROR_CODES.PRODUCT_EXTERNAL_ID, 'Missing product external ID');
      await voidRequest(ERROR_CODES.PRODUCT_EXTERNAL_ID, 'Missing product external ID', requestID);
      break;
    }
    case ERROR_CODES.API_ERROR: {
      errorWritter(ERROR_CODES.API_ERROR, `Error cause on API call, actual message:\n${msg}`);
      await voidRequest(ERROR_CODES.API_ERROR, msg, requestID);
      break;
    }
    case ERROR_CODES.MISSING_DATA: {
      errorWritter(ERROR_CODES.MISSING_DATA, `Data not found:\n${msg}`);
      await voidRequest(ERROR_CODES.MISSING_DATA, msg, requestID);
      break;
    }
    case ERROR_CODES.DATABASE_ERROR: {
      errorWritter(ERROR_CODES.DATABASE_ERROR, `Internal database fail\n${msg}`);
      break;
    }
    case ERROR_CODES.AUTH_CONNECT: {
      errorWritter(ERROR_CODES.AUTH_CONNECT, `QuickBooks auhtentification fail\n:${msg}`);
      break;
    }
    default: {
      errorWritter(error, `General error occured, actual error: \n${msg} `);
      await voidRequest(ERROR_CODES.DATABASE_ERROR, msg, requestID)
    }
  }
}


function errorWritter(code, message) {
  console.error(`Error code: ${code}, message: ${message}`)
}

async function voidRequest(code, errorMessage, requestID) {
  QBRequestsModel.update({
    status: code,
    error_msg: errorMessage
  }, {
    where: {
      request_uid: requestID
    }
  }).catch(err => {
    errorWritter(ERROR_CODES.DATABASE_ERROR, `Error updating Request table, actual error: ${err}`)
  });
}

module.exports = {
  ERROR_CODES,
  errorHandler
};