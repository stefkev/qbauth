const Sequelize =require('sequelize');
const DB_URL = process.env.DATABASE_URL;

const sequelize = new Sequelize(DB_URL, {
  logging: false,
  define: {
    timestamps: false,
  }
});

const CompanyExternalLinkModel = sequelize.import('../model/company_external_links');
const ProductExternalLinkModel = sequelize.import('../model/product_external_links');
const QBItemModel = sequelize.import('../model/qb_item');
const QBCompanyModel = sequelize.import('../model/qb_company');
const QBRequestsModel = sequelize.import('../model/qb_requests');
const QBTranslogDTLModel = sequelize.import('../model/qb_translog_dtl');
const QBTranslogMSTModel = sequelize.import('../model/qb_translog_mst');

sequelize.sync({force: false});

module.exports = {
  sequelize,
  CompanyExternalLinkModel,
  ProductExternalLinkModel,
  QBCompanyModel,
  QBRequestsModel,
  QBTranslogDTLModel,
  QBTranslogMSTModel,
  QBItemModel
};
