let QuickBooks = require('node-quickbooks');

let authenticate = (oauthToken, refreshToken) => {
    let sandbox = process.env.QB_ENVIRONMENT === 'sandbox';
      return new QuickBooks
    (
      process.env.QB_CLIENT_ID,
      process.env.QB_CLIENT_SECRET,
      oauthToken,
      false,
      process.env.QB_COMPANY_ID,
      sandbox,
      true,
      null,
      '2.0',
      refreshToken
    );
};

module.exports = {
  authenticate
};
