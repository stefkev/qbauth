const {customerDownload, itemDownload, vendorDownload, pushOrder, pushInvoice} = require('./ops');

/**
 * @constructor requestTypeSwitcher
 * @param inputData
 */
function requestTypeSwitcher (inputData) {
  inputData.forEach(async (data) => {
    let dataValues = data.dataValues;
    let requestUid = dataValues.request_uid;
    let requestType = dataValues.request_type;
    let accountName = dataValues.account_name;
    let className = dataValues.class_name;

    switch (requestType) {
      case 'CU':
        console.log('Dispatched CUSTOMER DOWNLOAD task');
        let customerDownloadTask = await customerDownload(requestUid);
        break;
      case 'VE':
        console.log('Dispatched VENDOR DOWNLOAD task');
        let vendorDownloadTask = await vendorDownload(requestUid);
        break;
      case 'IT':
        console.log('Dispatched ITEM DOWNLOAD task');
        let itemDownloadTask = await itemDownload(requestUid);
        break;
      case 'PO':
        console.log('Dispatched PUSH ORDER task');
        let pushOrderTask = await pushOrder(requestUid);
        break;
      case 'IN':
        let pushInvoiceData = await pushInvoice(requestUid, className);
        break;
      default:
        console.log(`Could not match request type in database, got ${requestType}`);
        return null;
    }
  });
}

module.exports = {
  requestTypeSwitcher
};
