let storeEnum = {
  stored: {
    optics: {}
  }
};

function checkIfAuthenticated () {
  return Object.length(storeEnum.stored.optics) !== 0;
}
let qbo = {};
module.exports = {
  storeEnum,
  checkIfAuthenticated,
  qbo
};
