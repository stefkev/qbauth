const EventEmitter = require('events');
const INTERVAL = process.env.REFRESH_INTERVAL;
const requestTypeSwitcher = require('./requestSwitch').requestTypeSwitcher;
let RequestModel = require('./database').QBRequestsModel;
const opticsAuthData = require("./util").storeEnum.stored.optics;
const oauthClient = require('../routes/qbOauth').oauthClient;


class ScanEmitter extends EventEmitter {}
const scanEmit = new ScanEmitter();

/**
 * emits scan event
 * @constructor
 */
scanEmit.on('scan', () => {
  if (Object.entries(opticsAuthData).length === 0) {
    console.log('Not yet authenticated, please authenticate');
    return 0;
  }
  console.log(`Scan emitted on ${Date.now()}`);
  RequestModel.findAll({
    where: { status : 0}
  }).then(data => {
    if(data.length === 0) {
      console.log('Nothing found in request table');
      return null;
    }
    requestTypeSwitcher(data);
    console.log('Interval scan called');
  })
});

/**
 * @constructor
 * interval scan on N seconds basis
 */
const intervalScan = () => {
  setInterval(() => {
    scanEmit.emit('scan');
  }, INTERVAL);
};

/**
 * @constructor
 * reauth interval  on N seconds basis
 */
const intervalReAuth = (timeout) => {
  setInterval(() => {
    oauthClient.refresh().then(data => {
      console.log(`Token refreshed with data: ${data}`)
    })
    .catch(err => {
      console.error(`Error occured on reauthentication: ${err}`)
    })
  }, timeout)
}

module.exports = {
  intervalScan,
  intervalReAuth
};





