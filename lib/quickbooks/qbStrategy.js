const QBRequestBuild = require('./qbRequestBuilder');
const util = require("../util");

/**
 * @class QBStrategy
 */
class QBStrategy {
  constructor() {
    this._accessToken = util.storeEnum.stored.optics.access_token;
    this._refreshToken = util.storeEnum.stored.optics.refresh_token;
  }

  /**
   * @param strategy
   * @param params
   * @returns {Promise<Array, Error>}
   */
  async doOperation(strategy, params) {
    let qbBuildInstance =  new QBRequestBuild(this._accessToken, this._refreshToken);
    qbBuildInstance.method(strategy);
    let buildMethod = qbBuildInstance.buildIt();
    return new Promise((resolve, reject) => {
      buildMethod(params, (err, data) => {
        if (err) return reject(err);
        return resolve(data);
      });
    });
  }
}

/**
 * @class QBContext
 * @param strategy
 */

class QBContext {
  constructor(strategy){
    this.strategy = strategy;
  }

  /**
   *
   * @param params
   * @returns {Promise<Array, Error>}
   */

  async executeStrategy(params) {
    return await new QBStrategy().doOperation(this.strategy, params);
  }
}

module.exports = QBContext;
