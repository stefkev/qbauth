const QBContext = require('./qbStrategy');
const {ERROR_CODES} = require('../errorHandler');

async function findVendors() {
  return await new QBContext('findVendors').executeStrategy({fetchAll: true}).catch(err => {
    throw {code: ERROR_CODES.API_ERROR, message: `Error thrown on findVendors, actual error:\n${err.Fault.Error[0].Detail}`}
  });
}

module.exports = {
  findVendors
};


