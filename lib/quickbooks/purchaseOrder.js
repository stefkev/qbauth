const QBContext = require('./qbStrategy');
const util = require('util');
const moment = require('moment');
const {ERROR_CODES} = require('../errorHandler');

/**
 * @constructor createPurchaseOrder
 * @returns {Promise<Object>}
 */

async function createPurchaseOrder(logFetched, className){
  const client = logFetched.company;
  const completeMST = logFetched.mst;
  const completeDTL = logFetched.dtl;

  const Line = [];

  const APARefValue = 35;
  const CostsOfGoodsSold = 31;
  const dueDate = moment(completeMST.order_date).add(15, 'days').format('YYYY-MM-DD');

  completeDTL.forEach(dtl => {
    Line.push({
      DetailType: "ItemBasedExpenseLineDetail",
      Amount: dtl.extprice,
      Description: dtl.item.desc,
      Id: dtl.lineitem,
      ItemBasedExpenseLineDetail: {
        ItemRef: {
          name: dtl.item.desc,
          value: dtl.item.external_id
        },
        TaxCodeRef: {
          value: "NON"
        },
        Qty: dtl.qty,
        UnitPrice: dtl.price,
        BillableStatus: "NotBillable"
      },
    });
  });

  const finalData = {
    DocNumber: completeMST.doc_id,
    domain: "QBO",
    APAccountRef: {
      value: APARefValue
    },
    CurrencyRef: {
      name: "United States Dollar",
      value: "USD"
    },
    TxnDate: completeMST.order_date,
    TotalAmt: completeMST.total_amount,
    POStatus: "Open",
    sparse: false,
    VendorRef: {
      value: completeMST.external_id
    },
    Line
  }

  console.log(util.inspect(finalData, {showHidden: false, depth: null}));

  return await new QBContext('createPurchaseOrder').executeStrategy(finalData).catch(err => {
    throw {code: ERROR_CODES.API_ERROR, message: `Error thrown on createPurchaseOrder, actual error:\n${err.Fault.Error[0].Detail}`}
  });
}

module.exports = {
  createPurchaseOrder
};
