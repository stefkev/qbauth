const util = require('util');
const moment = require('moment');
const QBContext = require('./qbStrategy');
const {ERROR_CODES} = require('../errorHandler');

async function findAll() {
  return await new QBContext('findItems').executeStrategy({fetchAll: true}).catch(err => {
    throw {code: ERROR_CODES.API_ERROR, message: `Error thrown on findItems, actual error:\n${err.Fault.Error[0].Detail}`}
  });
}

async function create(name, type, startQty, upc, price, dateCreated) {

  const finalType = type || 'Inventory';
  const qty = 0;

  const ID_SalesOfProductIncome = 53;
  const ID_InventoryAsset = 32;
  const ID_CostOfGoodsSold = 31;

  const finalData = {
    TrackQtyOnHand: true,
    PurchaseCost: price,
    Name: upc,
    Sku: upc,
    Description: name,
    IncomeAccountRef: {
      name: "Sales of Product Income",
      value: ID_SalesOfProductIncome
    },
    AssetAccountRef: {
      name: "Inventory Asset", 
      value: ID_InventoryAsset
    },
    ExpenseAccountRef: {
      name: "Cost of Goods Sold", 
      value: ID_CostOfGoodsSold
    },
    InvStartDate: dateCreated, 
    Type: finalType,
    QtyOnHand: qty, 
  }
  console.log(util.inspect(finalData, {showHidden: false, depth: null}));
  return await new QBContext('createItem').executeStrategy(finalData).catch(err => {
    throw {code: ERROR_CODES.API_ERROR, message: `Error thrown on createItem, actual error:\n${err.Fault.Error[0].Detail}`}
  });
}
module.exports = {
  findAll,
  create
};
