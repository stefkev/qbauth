const QuickBooks = require('node-quickbooks');

/**
 * @class QBRequestBuild
 */
class QBRequestBuild {
  constructor(oauthToken, refreshToken){
    const sandbox = process.env.QB_ENVIRONMENT === 'sandbox';
    this.methodName = '';
    this.qbo = new QuickBooks(
      process.env.QB_CLIENT_ID,
      process.env.QB_CLIENT_SECRET,
      oauthToken,
      false,
      process.env.QB_COMPANY_ID,
      sandbox,
      false,
      null,
      '2.0',
      refreshToken
    )
  }

  /**
   * @param methodName
   */
  method(methodName) {
    return this.methodName = methodName;
  }

  /**
   *
   * @returns {Object}
   */
  buildIt() {
    return this.qbo[this.methodName].bind(this.qbo);
  }
}

module.exports = QBRequestBuild;
