const QBContext = require('./qbStrategy');
const util = require('util');
const moment = require('moment');
const {ERROR_CODES} = require('../errorHandler');

/**
 * @constructor createInvoice
 * @returns {Promise<Object>}
 */

async function createInvoice(logFetched, className){
  const client = logFetched.company;
  const completeMST = logFetched.mst;
  const completeDTL = logFetched.dtl;
  const Line = [];

  const dueDate = moment(completeMST.order_date).add(completeMST.terms, 'days').format('YYYY-MM-DD');

  completeDTL.forEach(dtl => {
    Line.push({
      Description: dtl.product_desc,
      DetailType: "SalesItemLineDetail",
      SalesItemLineDetail: {
        TaxCodeRef: {
          value: "NON"
        },
        Qty: dtl.qty,
        UnitPrice: dtl.price,
        ItemRef: {
          name: dtl.item.desc,
          value: dtl.item.external_id
        }
      },
      LineNum: dtl.lineitem,
      Amount: dtl.extprice
    });
  });

  const finalData = {
      TxnDate: completeMST.order_date,
      domain: "QBO",
      PrintStatus: "NeedToPrint",
      TotalAmt: completeMST.total_amount,
      Line,
      DueDate: dueDate,
      ApplyTaxAfterDiscount: false,
      DocNumber: completeMST.doc_id,
      sparse: false,
      CustomerMemo: {
        value: `Sales order Ref#: ${completeMST.ref_id}. Thank you for Your business.`
      },
      Deposit: 0,
      Balance: completeMST.total_amount,
      CustomerRef: {
        name: client.company_name,
        value: client.external_id
      }
  };



  console.log(util.inspect(finalData, {showHidden: false, depth: null}));


  return await new QBContext('createInvoice').executeStrategy(finalData).catch(err => {
    throw {code:ERROR_CODES.API_ERROR, message: `Error thrown on createInvoice, actual error:\n${err.Fault.Error[0].Detail}`}
  });
}

module.exports = {
  createInvoice
};
