const QBContext = require('./qbStrategy');
const {ERROR_CODES} = require("../errorHandler");

/**
 * @constructor findAll
 * @returns {Promise<Object>}
 */
async function findAll() {
  return await new QBContext('findCustomers').executeStrategy({fetchAll: true}).catch(err => {
    throw {code: ERROR_CODES.API_ERROR, message: `Error thrown in findCustomers, actual error:\n${err.Fault.Error[0].Detail}`}
  });
}

module.exports = {
  findAll
};
