## Quickbooks - Optics360 integration

This script contains following features:

1. Customer data download
2. Vendor data download
3. Company data download
4. Create Push order
5. Create invoice
6. Basic oauth authentication ageings Quickbooks

Change .env file for following variables:

```
 QB_CLIENT_ID= Quickbooks client ID
 QB_CLIENT_SECRET= Quickbooks client secret
 QB_COMPANY_ID= Quickbooks company ID
 QB_REDIRECT= Quickbooks oauth redirect, change to yourhost/oauth/authredir
 QB_ENVIRONMENT= Quickbooks enviorement, set it to production or development
 REFRESH_INTERVAL= Refresh interval in reading request, in milliseconds
 DATABASE_URL= Postgres database url
 
 HTTP_PORT= HTTP port, if not set default is 3000
 HTTPS_PORT= HTTPS port, if not set default is 443
```


**When script starts, please authenticate on Quickbooks on following url:**
``
    yourhost:PORT/oauth/auth
``
