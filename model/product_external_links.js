/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('product_external_links', {
    external_uid: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    account_name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    product_uid: {
      type: DataTypes.STRING,
      allowNull: true
    },
    external_id: {
      type: DataTypes.STRING,
      allowNull: true
    },
    entered: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'product_external_links'
  });
};
