/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('qb_company', {
    rec_uid: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    company_name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    address: {
      type: DataTypes.STRING,
      allowNull: true
    },
    city: {
      type: DataTypes.STRING,
      allowNull: true
    },
    state: {
      type: DataTypes.STRING,
      allowNull: true
    },
    postal: {
      type: DataTypes.STRING,
      allowNull: true
    },
    account_name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    external_id: {
      type: DataTypes.STRING,
      allowNull: true
    },
    vendor: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    entered: {
      type: DataTypes.DATE,
      defaultValue: sequelize.NOW
    }
  }, {
    tableName: 'qb_company'
  });
};
