/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('company_external_links', {
    external_uid: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    account_name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    company_uid: {
      type: DataTypes.STRING,
      allowNull: true
    },
    external_customer_id: {
      type: DataTypes.STRING,
      allowNull: true
    },
    external_vendor_id: {
      type: DataTypes.STRING,
      allowNull: true
    },
    entered: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'company_external_links'
  });
};
