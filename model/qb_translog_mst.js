/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('qb_translog_mst', {
    mst_uid: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    request_uid: {
      type: DataTypes.STRING,
      allowNull: true
    },
    order_uid: {
      type: DataTypes.STRING,
      allowNull: true
    },
    order_date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    company_uid: {
      type: DataTypes.STRING,
      allowNull: true
    },
    total_amount: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    entered: {
      type: DataTypes.DATE,
      allowNull: true
    },
    modified: {
      type: DataTypes.DATE,
      allowNull: true
    },
    external_id: {
      type: DataTypes.STRING,
      allowNull: true
    },
    doc_id: {
      type: DataTypes.STRING,
      allowNull: true
    },
    terms: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    ref_id: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    tableName: 'qb_translog_mst'
  });
};
