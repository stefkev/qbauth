/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('qb_requests', {
    request_uid: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    request_type: {
      type: DataTypes.STRING,
      allowNull: true
    },
    class_name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    account_name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    processed: {
      type: DataTypes.DATE,
      defaultValue: sequelize.NOW
    },
    entered: {
      type: DataTypes.DATE,
      allowNull: true
    },
    enteredby: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    error_msg: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    tableName: 'qb_requests'
  });
};
