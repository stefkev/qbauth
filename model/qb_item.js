/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('qb_item', {
    rec_uid: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    item_desc: {
      type: DataTypes.STRING,
      allowNull: true
    },
    upc_number: {
      type: DataTypes.STRING,
      allowNull: true
    },
    account_name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    external_id: {
      type: DataTypes.STRING,
      allowNull: true
    },
    entered: {
      type: DataTypes.DATE,
      defaultValue: sequelize.NOW
    }
  }, {
    tableName: 'qb_item'
  });
};
