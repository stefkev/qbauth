/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('qb_translog_dtl', {
    dtl_uid: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    mst_uid: {
      type: DataTypes.STRING,
      allowNull: true
    },
    request_uid: {
      type: DataTypes.STRING,
      allowNull: true
    },
    orderdtl_uid: {
      type: DataTypes.STRING,
      allowNull: true
    },
    product_uid: {
      type: DataTypes.STRING,
      allowNull: true
    },
    lineitem: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    upc_number: {
      type: DataTypes.STRING,
      allowNull: true
    },
    product_desc: {
      type: DataTypes.STRING,
      allowNull: true
    },
    qty: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    price: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    extprice: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    external_id: {
      type: DataTypes.STRING,
      allowNull: true
    },
    entered: {
      type: DataTypes.DATE,
      allowNull: true
    },
    modified: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'qb_translog_dtl'
  });
};
